import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route, useNavigate } from "react-router-dom";
import Home from "./Pages/Home";
import Labs from "./Pages/Labs";
import Login from "./Pages/Login";
import Signup from "./Pages/Signup"
import Education from "./Pages/Education";
import NewPage from "./Pages/Newpage";

export default function App() {


  

  return (
    <BrowserRouter>
    
      <Routes>
          <Route path="/" element={<Home />} />
          <Route path="labs" element={<Labs />} />
          <Route path="education" element={<Education />} />
          <Route path="/login" element={<Login />} />
          <Route path="/signup" element={<Signup />} />
          <Route path="/newpage" element={<NewPage />} />

      </Routes>
    </BrowserRouter>
  );
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);