import React, { useRef, useState } from 'react';
import '../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
// import {useHistory} from 'react-router-dom'
import { BrowserRouter, Routes, Route, useNavigate } from "react-router-dom";
import NavbarSection from '../Components/Navbar/Navbar';
import Hero from '../Components/HeroSection/Hero';
import Middle from '../Components/MiddleSection/Middle';
import Select_Partners from '../Components/Select_Partners/Select_Partners';
import GetInTouch from '../Components/GetInTouch/GetInTouch';
import Footer from '../Components/Footer/Footer';
import NewHero from '../Components/NewHeroSection/NewHero';

import NewPage from './Newpage';
import { toasts, ToastContainer, FlatToast } from 'svelte-toasts';


const Signup = function(){

    // const navigate = useNavigate();

    const navigateToSignPage = () => {
      // 👇️ navigate to /
      navigate('/');
    };
 
    const [name, setName] = useState(); 
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const navigate = useNavigate();

async function handleSignup() {

    const user = { name, email, password };
    const jsonString = JSON.stringify(user);
    console.log('jsonString in signup page', jsonString);


    try {
        const response = await fetch('http://app.realtorstat.com:8021/usersystem/user/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: jsonString
        });
        console.log('response from signup page', response);
        if (response.ok) {
            console.log('Successfully registered');
            // location.assign('/login');
            toasts.success('Registered Successfully', { placement: 'top-right', theme: 'light' });

            const response = await fetch('http://app.realtorstat.com:8021/usersystem/user/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: jsonString
            });
            console.log('response from login req from signup page', response);
            const data = await response.json();
            console.log('data from login req on signup page', data);
            console.log("token:", data.token);


            if (response.ok) {
                // Example: navigate('/dashboard');
                localStorage.setItem('auth_user', data.token);
                sessionStorage.setItem("auth_user", data.token)
                console.log('res', response);
                console.log('Login successful');
                toasts.success('LoggedIn Successfully', { placement: 'top-right', theme: 'light' });
                // location.assign('/dashboard');
                
                const headers = {
                    Authorization: `Token ${data.token}`,
                    'Content-Type': 'application/json' 
                };

                // Send the GET request
                const response2 = await fetch("http://app.realtorstat.com:8021/api/list-orgs", {
                    method: 'GET',
                    headers: headers
                });
                console.log("GET Response",response2);
                const data2 = await response2.json();
                console.log("data2 from get req",data2);
                if (response2.ok) {
                    // throw redirect(302, '/dashboard');
                    // throw {
                    // 	redirect: '/dashboard',
                    // 	status: 302
                    // };

                    navigate('/');
                }
            } else {
                const responseData = await response.json();
                console.error('Login failed:', responseData.message);
            }
        } else {
            const responseData = await response.json();
            console.error('Registration failed in signup page:', responseData);
        }
    } catch (error) {
        console.error('An error occurred in signup:', error);
    }


}

    
return (
    <div className="newContainer">
    <div className="signup-form">
    <div className='form1'>
    
		<h4>Create an Account</h4>
		<p className="hint-text">Already have an acccount? <a href="/login" >Sign in?</a> </p>
        <div className="form-group">
			<div className="row">
				<div className="col">
                    <label for="first_name" className="name-lable" >Name</label>
                    <input type="text" className="form-control" name="first_name" onChange={(e)=>setName(e.target.value)} id="first_name" placeholder="First Name" required="required" /></div>
				
			</div>        	
        </div>
        <div className="form-group">
            <label for="email" className="name-lable" id="email" >Email</label>
        	<input type="email" className="form-control" name="email" onChange={(e)=>setEmail(e.target.value)} placeholder="Email" required="required" />
        </div>
		<div className="form-group">
            <label for="password" className="name-lable" id="password" >Password</label>
            <input type="password" className="form-control" name="password" onChange={(e)=>setPassword(e.target.value)} placeholder="Password" required="required" />
        </div>
		<div className="form-group">
            <label for="confirm_password" className="name-lable" >Confirm Password</label>
            <input type="password" className="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm Password" required="required" />
        </div>        
        <div className="form-group">
			<label className="form-check-label"><input type="checkbox" required="required" /> I Agree the <a href="#">Terms and Conditions</a></label>
            {/* &amp; <a href="#">Privacy Policy</a> */}
		</div>
		<div className="form-group">
            <button type="submit" onClick={handleSignup} className="btn btn-primary btn-lg btn-block ">Register Now</button>
          
            <button type="button" onClick={navigateToSignPage} className="btn btn-light mybtn">Cancel</button>
        </div>
  
    </div>
</div>
</div>
)

}


export default Signup;